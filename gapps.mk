#
# Copyright (C) 2020 AOSiP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

LOCAL_DEVICE := $(lastword $(subst _, ,$(TARGET_PRODUCT)))

# Overlays
PRODUCT_PACKAGE_OVERLAYS += \
    vendor/gapps/overlay/

# RRO Overlays
PRODUCT_PACKAGES += \
    PixelLauncherOverlay \
    PixelSetupWizardOverlay

PRODUCT_COPY_FILES += \
    vendor/gapps/privapp-permissions-pixel.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/permissions/privapp-permissions-pixel.xml \

$(call inherit-product, vendor/gapps/common/common-vendor.mk)
